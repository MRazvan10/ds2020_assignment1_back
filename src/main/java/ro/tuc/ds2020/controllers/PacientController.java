package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PacientDTO;
import ro.tuc.ds2020.dtos.PacientDetailsDTO;
import ro.tuc.ds2020.entities.Pacient;
import ro.tuc.ds2020.services.PacientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/pacient")
public class PacientController {

    private final PacientService pacientService;

    @Autowired
    public PacientController(PacientService pacientService) {
        this.pacientService = pacientService;
    }

    @GetMapping()
    public ResponseEntity<List<PacientDTO>> getPacients() {
        List<PacientDTO> dtos = pacientService.findPacients();
        for (PacientDTO dto : dtos) {
            Link pacientLink = linkTo(methodOn(PacientController.class)
                    .getPacient(dto.getId())).withRel("pacientDetails");
            dto.add(pacientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody PacientDetailsDTO pacientDTO) {
        UUID pacientID = pacientService.insert(pacientDTO);
        return new ResponseEntity<>(pacientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PacientDetailsDTO> getPacient(@PathVariable("id") UUID pacientId) {
        PacientDetailsDTO dto = pacientService.findPacientById(pacientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping (value="/{id}")
    public ResponseEntity<PacientDetailsDTO> deletep(@PathVariable("id") UUID pacientId){
        PacientDetailsDTO dto = pacientService.findPacientById(pacientId);
        pacientService.delete(pacientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<PacientDetailsDTO> updateProsumer(@PathVariable("id") UUID pacientId,@Valid @RequestBody PacientDetailsDTO pacientDTO) {
        PacientDetailsDTO details = pacientService.update(pacientId,pacientDTO);
        return new ResponseEntity<>(details, HttpStatus.CREATED);
    }

}
