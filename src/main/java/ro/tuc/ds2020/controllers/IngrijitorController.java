package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.IngrijitorDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.services.IngrijitorService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/ingrijitor")
public class IngrijitorController {

    private final IngrijitorService ingrijitorService;

    @Autowired
    public IngrijitorController(IngrijitorService ingrijitorService) {
        this.ingrijitorService = ingrijitorService;
    }

    @GetMapping()
    public ResponseEntity<List<IngrijitorDTO>> getIngrijitors() {
        List<IngrijitorDTO> dtos = ingrijitorService.findIngrijitors();
        for (IngrijitorDTO dto : dtos) {
            Link ingrijitorLink = linkTo(methodOn(IngrijitorController.class)
                    .getIngrijitor(dto.getId())).withRel("ingrijitorDetails");
            dto.add(ingrijitorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody IngrijitorDetailsDTO ingrijitorDTO) {
        UUID ingrijitorID = ingrijitorService.insert(ingrijitorDTO);
        return new ResponseEntity<>(ingrijitorID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<IngrijitorDetailsDTO> getIngrijitor(@PathVariable("id") UUID ingrijitorId) {
        IngrijitorDetailsDTO dto = ingrijitorService.findIngrijitorById(ingrijitorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping (value="/{id}")
    public ResponseEntity<IngrijitorDetailsDTO> deletep(@PathVariable("id") UUID ingrijitorId){
        IngrijitorDetailsDTO dto = ingrijitorService.findIngrijitorById(ingrijitorId);
        ingrijitorService.delete(ingrijitorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @PutMapping(value="/{id}")
    public ResponseEntity<IngrijitorDetailsDTO> updateProsumer(@PathVariable("id") UUID ingrijitorId, @Valid @RequestBody IngrijitorDetailsDTO ingrijitorDTO) {
        IngrijitorDetailsDTO details = ingrijitorService.update(ingrijitorId,ingrijitorDTO);
        return new ResponseEntity<>(details, HttpStatus.CREATED);
    }

}
