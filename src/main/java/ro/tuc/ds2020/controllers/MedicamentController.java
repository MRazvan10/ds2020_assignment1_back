package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicamentDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.services.MedicamentService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicament")
public class MedicamentController {

    private final MedicamentService medicamentService;

    @Autowired
    public MedicamentController(MedicamentService medicamentService) {
        this.medicamentService = medicamentService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicamentDTO>> getMedicaments() {
        List<MedicamentDTO> dtos = medicamentService.findMedicaments();
        for (MedicamentDTO dto : dtos) {
            Link medicamentLink = linkTo(methodOn(MedicamentController.class)
                    .getMedicament(dto.getId())).withRel("medicamentDetails");
            dto.add(medicamentLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody MedicamentDetailsDTO medicamentDTO) {
        UUID medicamentID = medicamentService.insert(medicamentDTO);
        return new ResponseEntity<>(medicamentID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicamentDetailsDTO> getMedicament(@PathVariable("id") UUID medicamentId) {
        MedicamentDetailsDTO dto = medicamentService.findMedicamentById(medicamentId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @PutMapping(value="/{id}")
    public ResponseEntity<MedicamentDetailsDTO> updateProsumer(@PathVariable("id") UUID medicamentId, @Valid @RequestBody MedicamentDetailsDTO medicamentDTO) {
        MedicamentDetailsDTO details = medicamentService.update(medicamentId,medicamentDTO);
        return new ResponseEntity<>(details, HttpStatus.CREATED);
    }

    @DeleteMapping (value="/{id}")
    public ResponseEntity<MedicamentDetailsDTO> deletep(@PathVariable("id") UUID medicamentId){
        MedicamentDetailsDTO dto = medicamentService.findMedicamentById(medicamentId);
        medicamentService.delete(medicamentId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
