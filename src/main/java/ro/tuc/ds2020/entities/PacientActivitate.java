package ro.tuc.ds2020.entities;


import javax.persistence.*;

@Entity
@Table(name = "activitatePacienti")
public class PacientActivitate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "pacientId", nullable = false)
    private Integer pacientId;

    @Column(name = "activitate", nullable = false)
    private String activitate;

    @Column(name = "startTime", nullable = false)
    private long startTime;

    @Column(name = "endTime", nullable = false)
    private long endTime;

    public PacientActivitate() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPacientId() {
        return pacientId;
    }

    public String getActivitate() {
        return activitate;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }
}