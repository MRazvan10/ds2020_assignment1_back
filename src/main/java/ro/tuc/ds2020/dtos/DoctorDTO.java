package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

public class DoctorDTO extends RepresentationModel<DoctorDTO> {
    private UUID id;
    private String name;
    private int age;
    private String email;
    private String password;

    public DoctorDTO() {
    }

    public DoctorDTO(UUID id, String name, int age,String email,String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email=email;
        this.password=password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return age == doctorDTO.age &&
                Objects.equals(name, doctorDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
