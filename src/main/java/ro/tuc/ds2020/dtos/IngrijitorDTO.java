package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

public class IngrijitorDTO extends RepresentationModel<IngrijitorDTO> {
    private UUID id;
    private String name;
    private String email;
    private String password;
    private int age;

    public IngrijitorDTO() {
    }

    public IngrijitorDTO(UUID id, String name, int age,String email,String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email=email;
        this.password=password;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngrijitorDTO ingrijitorDTO = (IngrijitorDTO) o;
        return age == ingrijitorDTO.age &&
                Objects.equals(name, ingrijitorDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
