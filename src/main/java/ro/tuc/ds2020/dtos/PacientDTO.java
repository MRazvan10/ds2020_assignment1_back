package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

public class PacientDTO extends RepresentationModel<PacientDTO> {
    private UUID id;
    private String name;
    private String address;
    private int age;
    private String email;
    private String password;
    private String diagnostic;
    private String idIngrijitor;
    private String tratament;


    public PacientDTO() {
    }

    public PacientDTO(UUID id, String name,String address, int age,String email,String password,String diagnostic,String idIngrijitor,String tratament) {
        this.id = id;
        this.name = name;
        this.address=address;
        this.age = age;
        this.email=email;
        this.password=password;
        this.diagnostic=diagnostic;
        this.idIngrijitor=idIngrijitor;
        this.tratament=tratament;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getIdIngrijitor() {
        return idIngrijitor;
    }

    public void setIdIngrijitor(String idIngrijitor) {
        this.idIngrijitor = idIngrijitor;
    }

    public String getTratament() {
        return tratament;
    }

    public void setTratament(String tratament) {
        this.tratament = tratament;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PacientDTO pacientDTO = (PacientDTO) o;
        return age == pacientDTO.age &&
                Objects.equals(name, pacientDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
