package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class PacientDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;
    @AgeLimit(limit = 3)
    private int age;
    @NotNull
    private String email;
    @NotNull
    private String password;
    @NotNull
    private String diagnostic;
    @NotNull
    private String idIngrijitor;
    private String tratament;


    public PacientDetailsDTO() {
    }

    public PacientDetailsDTO(UUID id, @NotNull String name, @NotNull String address, int age, @NotNull String email, @NotNull String password, @NotNull String diagnostic, @NotNull String idIngrijitor,String tratament) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.age = age;
        this.email = email;
        this.password = password;
        this.diagnostic = diagnostic;
        this.idIngrijitor = idIngrijitor;
        this.tratament=tratament;
    }

    public PacientDetailsDTO(@NotNull String name, @NotNull String address, int age, @NotNull String email, @NotNull String password, @NotNull String diagnostic, @NotNull String idIngrijitor,String tratament) {
        this.name = name;
        this.address = address;
        this.age = age;
        this.email = email;
        this.password = password;
        this.diagnostic = diagnostic;
        this.idIngrijitor = idIngrijitor;
        this.tratament=tratament;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getIdIngrijitor() {
        return idIngrijitor;
    }

    public void setIdIngrijitor(String idIngrijitor) {
        this.idIngrijitor = idIngrijitor;
    }

    public String getTratament() {
        return tratament;
    }

    public void setTratament(String tratament) {
        this.tratament = tratament;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PacientDetailsDTO that = (PacientDetailsDTO) o;
        return age == that.age &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, age);
    }
}
