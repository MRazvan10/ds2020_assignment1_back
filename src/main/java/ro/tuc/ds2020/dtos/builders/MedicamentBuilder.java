package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicamentDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.entities.Medicament;

public class MedicamentBuilder {

    private MedicamentBuilder() {
    }

    public static MedicamentDTO toMedicamentDTO(Medicament medicament) {
        return new MedicamentDTO(medicament.getId(), medicament.getName(),medicament.getDescriere());
    }

    public static MedicamentDetailsDTO toMedicamentDetailsDTO(Medicament medicament) {
        return new MedicamentDetailsDTO(medicament.getId(), medicament.getName(), medicament.getDescriere());
    }

    public static Medicament toEntity(MedicamentDetailsDTO medicamentDetailsDTO) {
        return new Medicament(medicamentDetailsDTO.getName(),
                medicamentDetailsDTO.getDescriere());
    }
}
