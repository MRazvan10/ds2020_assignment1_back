package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.IngrijitorDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.entities.Ingrijitor;

public class IngrijitorBuilder {

    private IngrijitorBuilder() {
    }

    public static IngrijitorDTO toIngrijitorDTO(Ingrijitor ingrijitor) {
        return new IngrijitorDTO(ingrijitor.getId(), ingrijitor.getName(), ingrijitor.getAge(),ingrijitor.getEmail(),ingrijitor.getPassword());
    }

    public static IngrijitorDetailsDTO toIngrijitorDetailsDTO(Ingrijitor ingrijitor) {
        return new IngrijitorDetailsDTO(ingrijitor.getId(), ingrijitor.getName(), ingrijitor.getAddress(), ingrijitor.getAge(),
                ingrijitor.getEmail(),
                ingrijitor.getPassword());
    }

    public static Ingrijitor toEntity(IngrijitorDetailsDTO ingrijitorDetailsDTO) {
        return new Ingrijitor(ingrijitorDetailsDTO.getName(),
                ingrijitorDetailsDTO.getAddress(),
                ingrijitorDetailsDTO.getAge(),
                ingrijitorDetailsDTO.getEmail(),
                ingrijitorDetailsDTO.getPassword());
    }
}
