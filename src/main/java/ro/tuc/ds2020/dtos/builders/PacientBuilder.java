package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PacientDTO;
import ro.tuc.ds2020.dtos.PacientDetailsDTO;
import ro.tuc.ds2020.entities.Pacient;

public class PacientBuilder {

    private PacientBuilder() {
    }

    public static PacientDTO toPacientDTO(Pacient pacient) {
        return new PacientDTO(pacient.getId(), pacient.getName(),pacient.getAddress(), pacient.getAge(),pacient.getEmail(),pacient.getPassword(),pacient.getDiagnostic(),pacient.getIdIngrijitor(),pacient.getTratament());
    }

    public static PacientDetailsDTO toPacientDetailsDTO(Pacient pacient) {
        return new PacientDetailsDTO(pacient.getId(), pacient.getName(), pacient.getAddress(), pacient.getAge(),pacient.getEmail(), pacient.getPassword(), pacient.getDiagnostic(),pacient.getIdIngrijitor(),pacient.getTratament());
    }

    public static Pacient toEntity(PacientDetailsDTO pacientDetailsDTO) {
        return new Pacient(pacientDetailsDTO.getName(),
                pacientDetailsDTO.getAddress(),
                pacientDetailsDTO.getAge(),
                pacientDetailsDTO.getEmail(),
                pacientDetailsDTO.getPassword(),
                pacientDetailsDTO.getDiagnostic(),
                pacientDetailsDTO.getIdIngrijitor(),
                pacientDetailsDTO.getTratament());
    }
}
