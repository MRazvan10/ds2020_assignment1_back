package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class MedicamentDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    private String descriere;

    public MedicamentDetailsDTO() {
    }

    public MedicamentDetailsDTO(String name, String descriere) {
        this.name = name;
        this.descriere = descriere;
    }

    public MedicamentDetailsDTO(UUID id, String name, String descriere) {
        this.id = id;
        this.name = name;
        this.descriere = descriere;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicamentDetailsDTO that = (MedicamentDetailsDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(descriere, that.descriere);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, descriere);
    }
}
