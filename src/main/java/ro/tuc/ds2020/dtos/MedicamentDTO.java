package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

public class MedicamentDTO extends RepresentationModel<MedicamentDTO> {
    private UUID id;
    private String name;
    private String descriere;

    public MedicamentDTO() {
    }

    public MedicamentDTO(UUID id, String name,String descriere) {
        this.id = id;
        this.name = name;
        this.descriere=descriere;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicamentDTO medicamentDTO = (MedicamentDTO) o;
        return Objects.equals(name, medicamentDTO.name)&&
                Objects.equals(descriere, medicamentDTO.descriere);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name,descriere);
    }
}
