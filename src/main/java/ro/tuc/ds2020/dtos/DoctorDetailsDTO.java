package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class DoctorDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @AgeLimit(limit = 30)
    private int age;
    @NotNull
    private String email;
    @NotNull
    private String password;
    public DoctorDetailsDTO() {
    }

    public DoctorDetailsDTO(UUID id, @NotNull String name, int age, @NotNull String email, @NotNull String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
    }

    public DoctorDetailsDTO(@NotNull String name, int age, @NotNull String email, @NotNull String password) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDetailsDTO that = (DoctorDetailsDTO) o;
        return age == that.age &&
                Objects.equals(name, that.name) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

}
