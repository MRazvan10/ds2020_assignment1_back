package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PacientDTO;
import ro.tuc.ds2020.dtos.PacientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PacientBuilder;
import ro.tuc.ds2020.entities.Pacient;
import ro.tuc.ds2020.repositories.PacientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PacientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PacientService.class);
    private final PacientRepository pacientRepository;

    @Autowired
    public PacientService(PacientRepository pacientRepository) {
        this.pacientRepository = pacientRepository;
    }

    public List<PacientDTO> findPacients() {
        List<Pacient> pacientList = pacientRepository.findAll();
        return pacientList.stream()
                .map(PacientBuilder::toPacientDTO)
                .collect(Collectors.toList());
    }

    public PacientDetailsDTO findPacientById(UUID id) {
        Optional<Pacient> prosumerOptional = pacientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Pacient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Pacient.class.getSimpleName() + " with id: " + id);
        }
        return PacientBuilder.toPacientDetailsDTO(prosumerOptional.get());
    }

    public PacientDetailsDTO delete(UUID id) {
        Optional<Pacient> prosumerOptional = pacientRepository.findById(id);
        PacientDetailsDTO details=PacientBuilder.toPacientDetailsDTO(prosumerOptional.get());
        pacientRepository.deleteById(id);
        return details;
    }

    public PacientDetailsDTO update(UUID id,PacientDetailsDTO pacientDTO) {
        Pacient pacient = PacientBuilder.toEntity(pacientDTO);
        pacient.setId(id);
        pacient = pacientRepository.save(pacient);
        return pacientDTO;
    }

    public UUID insert(PacientDetailsDTO pacientDTO) {
        Pacient pacient = PacientBuilder.toEntity(pacientDTO);
        pacient = pacientRepository.save(pacient);
        LOGGER.debug("Pacient with id {} was inserted in db", pacient.getId());
        return pacient.getId();
    }

}
