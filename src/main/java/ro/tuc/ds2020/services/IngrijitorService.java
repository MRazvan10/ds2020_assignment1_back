package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.IngrijitorDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.dtos.IngrijitorDetailsDTO;
import ro.tuc.ds2020.dtos.builders.IngrijitorBuilder;
import ro.tuc.ds2020.dtos.builders.IngrijitorBuilder;
import ro.tuc.ds2020.dtos.builders.IngrijitorBuilder;
import ro.tuc.ds2020.entities.Ingrijitor;
import ro.tuc.ds2020.entities.Ingrijitor;
import ro.tuc.ds2020.entities.Ingrijitor;
import ro.tuc.ds2020.repositories.IngrijitorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class IngrijitorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(IngrijitorService.class);
    private final IngrijitorRepository ingrijitorRepository;

    @Autowired
    public IngrijitorService(IngrijitorRepository ingrijitorRepository) {
        this.ingrijitorRepository = ingrijitorRepository;
    }

    public List<IngrijitorDTO> findIngrijitors() {
        List<Ingrijitor> ingrijitorList = ingrijitorRepository.findAll();
        return ingrijitorList.stream()
                .map(IngrijitorBuilder::toIngrijitorDTO)
                .collect(Collectors.toList());
    }

    public IngrijitorDetailsDTO findIngrijitorById(UUID id) {
        Optional<Ingrijitor> prosumerOptional = ingrijitorRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Ingrijitor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Ingrijitor.class.getSimpleName() + " with id: " + id);
        }
        return IngrijitorBuilder.toIngrijitorDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(IngrijitorDetailsDTO ingrijitorDTO) {
        Ingrijitor ingrijitor = IngrijitorBuilder.toEntity(ingrijitorDTO);
        ingrijitor = ingrijitorRepository.save(ingrijitor);
        LOGGER.debug("Ingrijitor with id {} was inserted in db", ingrijitor.getId());
        return ingrijitor.getId();
    }

    public IngrijitorDetailsDTO delete(UUID id) {
        Optional<Ingrijitor> prosumerOptional = ingrijitorRepository.findById(id);
        IngrijitorDetailsDTO details= IngrijitorBuilder.toIngrijitorDetailsDTO(prosumerOptional.get());
        ingrijitorRepository.deleteById(id);
        return details;
    }
    public IngrijitorDetailsDTO update(UUID id, IngrijitorDetailsDTO ingrijitorDTO) {
        Ingrijitor ingrijitor = IngrijitorBuilder.toEntity(ingrijitorDTO);
        ingrijitor.setId(id);
        ingrijitor = ingrijitorRepository.save(ingrijitor);
        return ingrijitorDTO;
    }

}
