package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicamentDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.dtos.MedicamentDetailsDTO;
import ro.tuc.ds2020.dtos.builders.MedicamentBuilder;
import ro.tuc.ds2020.dtos.builders.MedicamentBuilder;
import ro.tuc.ds2020.dtos.builders.MedicamentBuilder;
import ro.tuc.ds2020.entities.Medicament;
import ro.tuc.ds2020.entities.Medicament;
import ro.tuc.ds2020.entities.Medicament;
import ro.tuc.ds2020.repositories.MedicamentRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicamentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicamentService.class);
    private final MedicamentRepository medicamentRepository;

    @Autowired
    public MedicamentService(MedicamentRepository medicamentRepository) {
        this.medicamentRepository = medicamentRepository;
    }

    public List<MedicamentDTO> findMedicaments() {
        List<Medicament> medicamentList = medicamentRepository.findAll();
        return medicamentList.stream()
                .map(MedicamentBuilder::toMedicamentDTO)
                .collect(Collectors.toList());
    }

    public MedicamentDetailsDTO findMedicamentById(UUID id) {
        Optional<Medicament> prosumerOptional = medicamentRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medicament with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medicament.class.getSimpleName() + " with id: " + id);
        }
        return MedicamentBuilder.toMedicamentDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(MedicamentDetailsDTO medicamentDTO) {
        Medicament medicament = MedicamentBuilder.toEntity(medicamentDTO);
        medicament = medicamentRepository.save(medicament);
        LOGGER.debug("Medicament with id {} was inserted in db", medicament.getId());
        return medicament.getId();
    }

    public MedicamentDetailsDTO delete(UUID id) {
        Optional<Medicament> prosumerOptional = medicamentRepository.findById(id);
        MedicamentDetailsDTO details= MedicamentBuilder.toMedicamentDetailsDTO(prosumerOptional.get());
        medicamentRepository.deleteById(id);
        return details;
    }

    public MedicamentDetailsDTO update(UUID id, MedicamentDetailsDTO medicamentDTO) {
        Medicament medicament = MedicamentBuilder.toEntity(medicamentDTO);
        medicament.setId(id);
        medicament = medicamentRepository.save(medicament);
        return medicamentDTO;
    }

}
