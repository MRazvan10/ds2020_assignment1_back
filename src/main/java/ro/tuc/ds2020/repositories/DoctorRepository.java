package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Doctor;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DoctorRepository extends JpaRepository<Doctor, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Doctor> findByName(String name);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Doctor p " +
            "WHERE p.name = :name " +
            "AND p.age >= 60  ")
    Optional<Doctor> findSeniorsByName(@Param("name") String name);

}
