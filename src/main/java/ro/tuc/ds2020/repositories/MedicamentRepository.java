package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Medicament;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MedicamentRepository extends JpaRepository<Medicament, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Medicament> findByName(String name);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Medicament p " +
            "WHERE p.name = :name ")
    Optional<Medicament> findSeniorsByName(@Param("name") String name);

}
