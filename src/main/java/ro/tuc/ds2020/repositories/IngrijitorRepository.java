package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Ingrijitor;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IngrijitorRepository extends JpaRepository<Ingrijitor, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Ingrijitor> findByName(String name);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Ingrijitor p " +
            "WHERE p.name = :name " +
            "AND p.age >= 60  ")
    Optional<Ingrijitor> findSeniorsByName(@Param("name") String name);

}
