package ro.tuc.ds2020.consumer;

public class ConsumerStart {

    private ConsumerStart() {
    }

    public static void startServer() {
        FileConsumer consumer = new FileConsumer();
        try {
            consumer.consume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

