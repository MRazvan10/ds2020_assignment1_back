package ro.tuc.ds2020.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import ro.tuc.ds2020.entities.PacientActivitate;

public class FileConsumer {
    private long timpBaie = 0;

    public FileConsumer() {
    }

    public void consume() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare("Tema2", false, false, false, null);
        System.out.println("Waiting for messages...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            PacientActivitate pacientActivity = new ObjectMapper().readValue(delivery.getBody(), PacientActivitate.class);
            System.out.println("Received : " + message);
            long hours = (pacientActivity.getEndTime() - pacientActivity.getStartTime()) / 1000 / 60 / 60;
            long minutes = ((pacientActivity.getEndTime() - pacientActivity.getStartTime()) / 1000 / 60) % 60;
            long seconds = ((pacientActivity.getEndTime() - pacientActivity.getStartTime()) / 1000) % 60;
            System.out.println(pacientActivity.getActivitate() + " " + hours + "h " + minutes + "min " + seconds + "sec");
            long time = pacientActivity.getEndTime() - pacientActivity.getStartTime();

            if (pacientActivity.getActivitate().equals("Toileting") || pacientActivity.getActivitate().equals("Grooming") || pacientActivity.getActivitate().equals("Showering")) {
                timpBaie += time;
            } else {
                timpBaie = 0;
            }

            if (pacientActivity.getActivitate().equals("Sleeping") && time > 1000 * 60 * 60 * 7) {
                System.out.println("Pacientul " + pacientActivity.getPacientId() + " doarme prea mult!");
            } else if (pacientActivity.getActivitate().equals("Leaving") && time > 1000 * 60 * 60 * 5) {
                System.out.println("Pacientul " + pacientActivity.getPacientId() + " este plecat de prea mult timp!");
            } else if (timpBaie > 1000 * 60 * 30) {
                System.out.println("Pacientul " + pacientActivity.getPacientId() + " sta prea mult in baie!");
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        channel.basicConsume("Tema2", true, deliverCallback, consumerTag -> {
        });
    }
}